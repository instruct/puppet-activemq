# class activemq::ssl

class activemq::ssl (
  $truststore_password,
  $keystore_password,
) {

  file { '/etc/activemq/ssl':
    ensure => directory,
    mode   => '0644',
    owner  => 'root',
    group  => 'root',
  }

  java_ks { 'puppet_ca_truststore':
    ensure       => latest,
    certificate  => '/etc/puppetlabs/puppet/ssl/certs/ca.pem',
    target       => '/etc/activemq/ssl/truststore.ts',
    password     => $truststore_password,
    trustcacerts => true,
  }

  java_ks { 'puppet_agent_keystore':
    ensure      => latest,
    certificate => "/etc/puppetlabs/puppet/ssl/certs/${::trusted['certname']}.pem",
    private_key => "/etc/puppetlabs/puppet/ssl/private_keys/${::trusted['certname']}.pem",
    target      => '/etc/activemq/ssl/keystore.ks',
    password    => $keystore_password,
  }

}
